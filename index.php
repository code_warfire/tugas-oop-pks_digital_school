    <?php
        require_once('animal.php');
        require_once('frog.php');
        require_once('Ape.php');


        $sheep = new Animal("shaun");

        echo "Animal Name : ".$sheep-> name."<br>"; // "shaun"
        echo "Number of animal legs : ".$sheep-> legs."<br>"; // 4
        echo "Are animals cold-blooded ? : ".$sheep-> cold_blooded."<br><br>"; // "no"

        $kodok = new Frog("buduk");

        echo "Animal Name : ".$kodok->name."<br>"; //buduk
        echo "Number of animal legs : ".$kodok-> legs."<br>"; // 4
        echo "Are animals cold-blooded ? : ".$kodok-> cold_blooded."<br>"; // "no"
        echo "Jump : ".$kodok->jump()."<br><br>"; // "hop hop"

        $sungokong = new Ape("kera sakti");
        
        echo "Animal Name : ".$sungokong->name."<br>"; //kera sakti
        echo "Number of animal legs : ".$sungokong-> legs."<br>"; // 4
        echo "Are animals cold-blooded ? : ".$sungokong-> cold_blooded."<br>"; // "no"
        echo "yell : ".$sungokong->yell()."<br><br>"; //  "Auooo"
        
        // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
    ?>
